var treeList
var treeListtmp
var trind

function shiftInd(lORr){
  if(lORr){
    trind= (trind+1)%strings.length
  } else {
    if(trind == 0){
      trind = strings.length - 1
    } else {
      trind = trind-1
    }
  }
  console.log(trind)
}

function initTree(strs){
  collapsed = false
  trind = 0
  strings = makestr(strs.length-1)
  treeListtmp = []
  for(var i = 0; i <strings.length; i++){
    var newtree = makeTree(strings[i], strs)
    newtree.collapse()
    treeListtmp.push(newtree)
  }
  treeList = treeListtmp
}

function setup() {
  createCanvas(screen.width, screen.height);
  treeList = []
  strs = "jji"
  trind = 0
  initTree(strs)
}

function draw() {
  background(255)
  drawTree(treeList[trind])
}

drawTree = function(tree){
  stroke(0)
  fill(0)
  strokeWeight(1)
  drawHelper(screen.width/2, 30, tree.root, screen.width/2, 30,10)

}

drawHelper = function(col, hgt, node, prevcol, prevhgt, lv){
  if(node.leaf){
    fill(255,0,0)
    ellipse(col, hgt, 20,20)
    text(node.value.val, col, hgt+30)
  } else {
    stroke(0)
    fill(0)
    ellipse(col,hgt, 15, 15)


    if(node.value.negative ){neg = "- ";}
    else{neg = "";}
    text(neg+node.value.val, col-20,hgt-10)


    drawHelper((col+screen.width/lv), hgt+100, node.right, col,hgt, lv*2)
    drawHelper((col-screen.width/lv), hgt+100, node.left, col,hgt, lv*2)
  }
  line(col,prevhgt,prevcol,prevhgt)
  line(col,hgt,col,prevhgt)
}
