function evec(){
  this.type = "vector";
  this.val = 'o'; // epsillon
  this.cross = function(invec){
    return new evec();
  }
}

function jvec(){
  this.type = "vector";
  this.val = 'j';
  this.negative = false;
  this.cross = function(invec){
    switch(invec.val) {
    case 'i':
        var out = new kvec();
        out.negative = !(invec.val.negative ^ this.negative);
        return out
        break;
    case 'j' || 'o':
        return new evec();
        break;
    case 'k':
        var out = new ivec();
        out.negative = (invec.val.negative ^ this.negative);
        return out;
        break;
    default:
        return new evec()
    }
  }
}

function kvec(){
  this.type = "vector";
  this.val = 'k';
  this.negative = false;
  this.cross = function(invec){
    switch(invec.val) {
    case 'i':
        var out = new jvec();
        out.negative = (invec.val.negative ^ this.negative);
        return out;
        break;
    case 'j':
        var out = new ivec();
        out.negative = !(invec.val.negative ^ this.negative);
        return out;
        break;
    case 'k'  || 'o' :
        return new evec();
        break;
    default:
        return new evec()
    }
  }
}

function ivec(){
  this.type = "vector";
  this.val = 'i';
  this.negative = false;
  this.cross = function(invec){
    switch(invec.val) {
    case 'i':
        return new evec();
        break;
    case 'j':
        var out = new kvec();
        out.negative = (invec.val.negative ^ this.negative);
        return out;
        break;
    case 'k'  :
        var out = new jvec();
        out.negative = !(invec.val.negative ^ this.negative);
        return out;
        break;
    default:
        return new evec()
    }
  }
}
