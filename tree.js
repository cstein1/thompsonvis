function Node(){
  this.value = null
  this.left = null
  this.right = null
  this.isright = false
  this.parent = null
  this.marked = false
  this.leaf = false
  this.createCarat = function(){
    this.marked = true
    this.left = new Node()
    this.left.parent = this
    this.left.marked = true

    this.right = new Node()
    this.right.parent = this
    this.right.isright = true
    return this.left
  }

}

function Tree(invec){
  this.root = new Node()
  this.root.marked = true
  this.invectmp = invec
  this.invec = invec
  this.fillVecs = function(){
    this.invec = this.invectmp
    this.fillVecsHelper(this.root)
    console.log("__)__")
    this.invec = this.invectmp
  }
  this.fillVecsHelper = function(node){
    console.log(node)
    if(this.invec.length == 0) {
      return
    }
    if(!(node.left instanceof Node) && !(node.right instanceof Node)){
      console.log("leaf")
      node.leaf = true
      tmp = 0
      switch(this.invec[0]){
        case 'i':
          tmp = new ivec()
          break;
        case 'j':
          tmp = new jvec()
          break;
        case 'k':
          tmp = new kvec()
          break;
        default:
          tmp = new evec()
      }
      tmp.negative=false
      node.value = tmp
      this.invec = this.invec.slice(1)
      return
    } else{
      this.fillVecsHelper( node.left )
      this.fillVecsHelper( node.right )
    }
  }
  this.collapse = function(){
    if(this.root.leaf){
      return this.root.value
    }
    leftcollapse = this.collapseH(this.root.left)
    rightcollapse = this.collapseH(this.root.right)
    this.root.value = leftcollapse.cross(rightcollapse)
    return this.root.value
  }
  this.collapseH = function(node){
    // Everything is named weird because
    // JAVASCRIPT SCOPE IS INSANE
    if(node.leaf){ // ivec
      return(node.value)
    }
    if(node.left.leaf && node.right.leaf){
      let lval1 = node.left.value
      let rval1 = node.right.value
      node.value = lval1.cross(rval1)
      return node.value
    } else if(node.left.leaf){
      let lval2 = node.left.value
      let rval2 = this.collapseH(node.right)
      node.value = lval2.cross(rval2)
      return node.value
    } else if(node.right.leaf){
      let lval3 = this.collapseH(node.left)
      let rval3 = node.right.value
      node.value = lval3.cross(rval3)
      return node.value
    } else{
      let leftcollapse = this.collapseH(node.left)
      let rightcollapse = this.collapseH(node.right)
      node.value = leftcollapse.cross(rightcollapse)
      return node.value
    }

  }
}
