makestr = function(leng){
  strs = []
  mkStrings = function(leng){
    mkStringsHelp(leng*2,1,0,"E")
  }

  mkStringsHelp = function(leng,Elen,Nlen,string){
    if(string.length >= leng){
      strs.push(string)
    } else {
      if(Nlen < Elen || Elen == leng/2){
        mkStringsHelp(leng,Elen,Nlen+1,string+"N")
      }
      if(Elen < leng/2){
        mkStringsHelp(leng,Elen+1,Nlen,string+"E")
      }
    }
  }
  mkStrings(leng)
  return strs
}

testing = function(){
  for(var i = 0; i< 10; i++){
    console.log("for i = "+i + " we have " + makestr(i).length)
  }

}
//testing()
