
makeTree = function(path,invec){
  tree = new Tree(invec)
  makeTreeH(path,tree.root)
  tree.fillVecs()
  return tree
}

makeTreeH = function(path,node){
  if(path.length <= 0){
    return
  } else if(path[0] == 'E'){
    newnode = node.createCarat()
    return makeTreeH(path.slice(1),newnode)
  } else if(path[0] == 'N'){
    if(node.isright){
      newnode = findNextRight(node) // Was passed "path"
      return makeTreeH(path.slice(1), newnode)
    } else{
      newnode = node.parent.right
      newnode.marked = true
      return makeTreeH(path.slice(1),newnode)
    }
  }
}

findNextRight = function(node){ // was passed inv
  tmp = node.parent
  top = 1
  while(tmp.right.marked){
    tmp = tmp.parent
    top+=1
  }
  newnode = tmp.right
  newnode.marked = true
  return newnode
}
