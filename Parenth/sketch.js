// TODO Replace MakeParTree with MakeParTrees which should return a list of tuple (inputleafAssignment, RootValue) and
// then prints out the result of the vector association theorem.

// TODO Hover over the assignments to see the trees to manually verify the algorithms


var treevis
var rootlist
var table_html

function makeParTreesList(strs){
  let invec = ""
  for(var i = 0; i < strs.length; i++){
    if(strs[i] != '(' && strs[i] != ')') invec+=strs[i]
  }
  rootlist = makeParTreeRoots(strs, invec).filter(x=>x[1]!='o')
  var newtree = makeParTree(strs, invec)
  treevis = newtree
}

function setup() {
  createCanvas(screen.width, screen.height);
  strs = "((vv)v)"
  //initTree(strs)
  makeParTreesList(strs)
}

function draw() {
  background(255)
  drawRootList()
  drawTree(treevis)
}

drawRootList = function(){
  stroke(0)
  fill(0)
  strokeWeight(.6)
  table_html = ""
  table_html= "<table><tr><td style='width: 100px; color: red;'>Vector Assignment</td>";
  table_html+= "<td style='width: 100px; color: red; text-align: right;'>i</td>";
  table_html+="<td style='width: 100px; color: red; text-align: right;'>j</td></tr>";

  table_html+="<tr><td style='width: 100px;                   '>---------------</td>";
  table_html+="<td     style='width: 100px; text-align: right;'>---------------</td>";
  table_html+="<td     style='width: 100px; text-align: right;'>---------------</td></tr>";

  for (var i=0; i<8; i++) {
    table_html+="<tr><td style='width: 100px;'>Number " + i + " is:</td>";
    table_html+="<td style='width: 100px; text-align: right;'>" +1 + "</td>";
    table_html+="<td style='width: 100px; text-align: right;'>" + 1 + "</td></tr>";
  }
   table_html+="</table>";
  for(let i = 0; i < rootlist.length; i++){
    text(rootlist[i][0].split('').join(' ')+", with the association above, has product "+rootlist[i][1], 20,screen.height/10+15*i)
  }
}



drawTree = function(tree){
  stroke(0)
  fill(0)
  strokeWeight(1)
  drawHelper(screen.width/2, 30, tree.root, screen.width/2, 30,10)

}

drawHelper = function(col, hgt, node, prevcol, prevhgt, lv){
  if(node.leaf){
    fill(255,0,0)
    ellipse(col, hgt, 20,20)
    //text(node.value.val, col, hgt+30)
  } else {
    stroke(0)
    fill(0)
    ellipse(col,hgt, 15, 15)


    if(node.value.negative ){neg = "- ";}
    else{neg = "";}
    //text(neg+node.value.val, col-20,hgt-10)


    drawHelper((col+screen.width/lv), hgt+100, node.right, col,hgt, lv*2)
    drawHelper((col-screen.width/lv), hgt+100, node.left, col,hgt, lv*2)
  }
  line(col,prevhgt,prevcol,prevhgt)
  line(col,hgt,col,prevhgt)
}
