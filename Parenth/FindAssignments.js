
FindAssignments = function(num){
  let strs = ['i','j','k']
  let lst = upToN3(num)//tree.invec.length)
  lst = lst.map(x=>x.map(y=>y=strs[y]).join(""))
  return lst
}

upToN3 = function(num){
  let digits = [0, 1, 2]
  let vecs = []
  help = function(current, numDigits) {
    if(numDigits==0){
      current = current.toString().split("").map(Number)
      while(current.length < num){
        current.unshift(0)
      }
      vecs.push(current)
    }
    else {
      for(let i = 0; i < digits.length; i++){
        help(current*10+digits[i], numDigits-1)
      }
    }
  }
  help(0, num)
  return vecs
}
