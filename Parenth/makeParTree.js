
makeParTree = function(path,invec){
  tree = new ParTree(invec)
  let rov = 0
  let curr = tree.root // Current node
  let seen = 1
  while(rov < path.length){
    switch(path[rov]){
      case '(':
        curr.marked = true
        curr.createCarat()
        curr.right.marked = false
        curr = curr.left
        curr.marked = true
        break;
      case ')':
        break;
      default:
        if(path[rov] == 'i')
          curr.value = new ivec()
        else if(path[rov]=='j')
          curr.value = new jvec()
        else if(path[rov]=='k')
          curr.value = new kvec()
        else curr.value = new evec()
        curr.leaf = true
        if(seen != invec.length){
          curr = findNextRight(curr)
          curr.marked = true
          seen++
        }
        break;
    }
    rov++
  }
  tree.collapse()
  return tree
}

findNextRight = function(node){
  tmp = node.parent
  top = 1
  while(tmp.right.marked){
    tmp = tmp.parent
    top+=1
  }
  newnode = tmp.right
  newnode.marked = true
  return newnode
}

makeParTreeRoots = function(path,invec){ // invec is vvv
  let assignments = FindAssignments(invec.length)
  let roots = []
  for(let tr = 0 ; tr < assignments.length; tr++)
  {
    tree = new ParTree(invec)
    tree.invec = assignments[tr]
    tree.invectmp = assignments[tr]
    let rov = 0
    let curr = tree.root // Current node
    let seen = 1
    let repvecind = 0
    while(rov < path.length){
      switch(path[rov]){
        case '(':
          curr.marked = true
          curr.createCarat()
          curr.right.marked = false
          curr = curr.left
          curr.marked = true
          break;
        case ')':
          break;
        default:
          if(tree.invec[repvecind] == 'i')
            curr.value = new ivec()
          else if(tree.invec[repvecind]=='j')
            curr.value = new jvec()
          else if(tree.invec[repvecind]=='k')
            curr.value = new kvec()
          else {
            curr.value = new evec()
          }
          repvecind++
          curr.leaf = true
          if(seen != invec.length){
            curr = findNextRight(curr)
            curr.marked = true
            seen++
          }
          break;
      }
      rov++
    }
    let rut = tree.collapse()
    let negstr = (rut.negative)?"-":""
    roots.push([assignments[tr],negstr+rut.val])
  }
  return roots
}
